
package net.mcreator.fantasymobs.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.block.BlockState;

import net.mcreator.fantasymobs.FantasyMobsModElements;

@FantasyMobsModElements.ModElement.Tag
public class BeholderPsiBoltAmmoItem extends FantasyMobsModElements.ModElement {
	@ObjectHolder("fantasy_mobs:beholder_psi_bolt_ammo")
	public static final Item block = null;
	public BeholderPsiBoltAmmoItem(FantasyMobsModElements instance) {
		super(instance, 49);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}
	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(null).maxStackSize(64));
			setRegistryName("beholder_psi_bolt_ammo");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public int getUseDuration(ItemStack itemstack) {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}
	}
}
