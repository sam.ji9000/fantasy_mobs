
package net.mcreator.fantasymobs.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.entity.LivingEntity;
import net.minecraft.block.BlockState;

import net.mcreator.fantasymobs.procedures.DrowWandBulletHitProcedure;
import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;
import java.util.HashMap;

@FantasyMobsModElements.ModElement.Tag
public class DrowWandBulletItem extends FantasyMobsModElements.ModElement {
	@ObjectHolder("fantasy_mobs:drow_wand_bullet")
	public static final Item block = null;
	public DrowWandBulletItem(FantasyMobsModElements instance) {
		super(instance, 54);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}
	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(null).maxStackSize(64));
			setRegistryName("drow_wand_bullet");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public int getUseDuration(ItemStack itemstack) {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}

		@Override
		public boolean hitEntity(ItemStack itemstack, LivingEntity entity, LivingEntity sourceentity) {
			boolean retval = super.hitEntity(itemstack, entity, sourceentity);
			double x = entity.getPosX();
			double y = entity.getPosY();
			double z = entity.getPosZ();
			World world = entity.world;
			{
				Map<String, Object> $_dependencies = new HashMap<>();
				$_dependencies.put("itemstack", itemstack);
				DrowWandBulletHitProcedure.executeProcedure($_dependencies);
			}
			return retval;
		}
	}
}
