
package net.mcreator.fantasymobs.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.UseAction;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.Food;

import net.mcreator.fantasymobs.itemgroup.FantasyMobsItemGroup;
import net.mcreator.fantasymobs.FantasyMobsModElements;

@FantasyMobsModElements.ModElement.Tag
public class DriedMushroomItem extends FantasyMobsModElements.ModElement {
	@ObjectHolder("fantasy_mobs:dried_mushroom")
	public static final Item block = null;
	public DriedMushroomItem(FantasyMobsModElements instance) {
		super(instance, 15);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new FoodItemCustom());
	}
	public static class FoodItemCustom extends Item {
		public FoodItemCustom() {
			super(new Item.Properties().group(FantasyMobsItemGroup.tab).maxStackSize(64)
					.food((new Food.Builder()).hunger(4).saturation(1f).setAlwaysEdible().build()));
			setRegistryName("dried_mushroom");
		}

		@Override
		public int getUseDuration(ItemStack stack) {
			return 10;
		}

		@Override
		public UseAction getUseAction(ItemStack par1ItemStack) {
			return UseAction.EAT;
		}
	}
}
