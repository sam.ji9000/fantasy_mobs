
package net.mcreator.fantasymobs.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.block.BlockState;

import net.mcreator.fantasymobs.itemgroup.FantasyMobsItemGroup;
import net.mcreator.fantasymobs.FantasyMobsModElements;

@FantasyMobsModElements.ModElement.Tag
public class HorrorHeadItem extends FantasyMobsModElements.ModElement {
	@ObjectHolder("fantasy_mobs:horror_head")
	public static final Item block = null;
	public HorrorHeadItem(FantasyMobsModElements instance) {
		super(instance, 76);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}
	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(FantasyMobsItemGroup.tab).maxStackSize(64));
			setRegistryName("horror_head");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public int getUseDuration(ItemStack itemstack) {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}
	}
}
