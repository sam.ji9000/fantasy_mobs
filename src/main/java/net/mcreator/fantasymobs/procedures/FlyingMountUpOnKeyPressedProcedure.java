package net.mcreator.fantasymobs.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.entity.FlyingMountEntity;
import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;

@FantasyMobsModElements.ModElement.Tag
public class FlyingMountUpOnKeyPressedProcedure extends FantasyMobsModElements.ModElement {
	public FlyingMountUpOnKeyPressedProcedure(FantasyMobsModElements instance) {
		super(instance, 38);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FlyingMountUpOnKeyPressed!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (((entity.getRidingEntity()) instanceof FlyingMountEntity.CustomEntity)) {
			(entity.getRidingEntity()).setMotion(((entity.getRidingEntity()).getMotion().getX()), 1, ((entity.getRidingEntity()).getMotion().getZ()));
		}
	}
}
