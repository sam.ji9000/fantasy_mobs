package net.mcreator.fantasymobs.procedures;

import net.minecraft.util.Hand;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;

@FantasyMobsModElements.ModElement.Tag
public class FlyingMountOnEntityTickUpdateProcedure extends FantasyMobsModElements.ModElement {
	public FlyingMountOnEntityTickUpdateProcedure(FantasyMobsModElements instance) {
		super(instance, 45);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FlyingMountOnEntityTickUpdate!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof LivingEntity) {
			((LivingEntity) entity).swing(Hand.MAIN_HAND, true);
		}
		if (entity instanceof LivingEntity) {
			((LivingEntity) entity).swing(Hand.OFF_HAND, true);
		}
		if ((entity.isBeingRidden())) {
			if (((entity.getPersistentData().getDouble("secondsUnmounted")) != 0)) {
				entity.getPersistentData().putDouble("secondsUnmounted", 0);
			}
		} else {
			entity.getPersistentData().putDouble("secondsUnmounted", ((entity.getPersistentData().getDouble("secondsUnmounted")) + 0.05));
			if (((entity.getPersistentData().getDouble("secondsUnmounted")) >= 10)) {
				if (!entity.world.isRemote)
					entity.remove();
			}
		}
	}
}
