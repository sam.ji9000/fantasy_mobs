package net.mcreator.fantasymobs.procedures;

import net.minecraft.util.Hand;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;

@FantasyMobsModElements.ModElement.Tag
public class HookHorrorPlayerCollidesWithThisEntityProcedure extends FantasyMobsModElements.ModElement {
	public HookHorrorPlayerCollidesWithThisEntityProcedure(FantasyMobsModElements instance) {
		super(instance, 73);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure HookHorrorPlayerCollidesWithThisEntity!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof LivingEntity) {
			((LivingEntity) entity).swing(Hand.MAIN_HAND, true);
		}
		if (entity instanceof LivingEntity) {
			((LivingEntity) entity).swing(Hand.OFF_HAND, true);
		}
	}
}
