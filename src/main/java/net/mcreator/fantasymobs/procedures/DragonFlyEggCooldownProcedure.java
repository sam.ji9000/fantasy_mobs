package net.mcreator.fantasymobs.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;

@FantasyMobsModElements.ModElement.Tag
public class DragonFlyEggCooldownProcedure extends FantasyMobsModElements.ModElement {
	public DragonFlyEggCooldownProcedure(FantasyMobsModElements instance) {
		super(instance, 48);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure DragonFlyEggCooldown!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((entity.getPersistentData().getBoolean("summonMountInCD"))) {
			entity.getPersistentData().putDouble("summonMountCD", ((entity.getPersistentData().getDouble("summonMountCD")) + 0.05));
			if (((entity.getPersistentData().getDouble("summonMountCD")) >= 10)) {
				entity.getPersistentData().putBoolean("summonMountInCD", (false));
				entity.getPersistentData().putDouble("summonMountCD", 0);
			}
		}
	}
}
