package net.mcreator.fantasymobs.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.IWorld;
import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.world.dimension.UnderdarkDimension;
import net.mcreator.fantasymobs.FantasyMobsModVariables;
import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;
import java.util.HashMap;

@FantasyMobsModElements.ModElement.Tag
public class OnModLoadedProcedure extends FantasyMobsModElements.ModElement {
	public OnModLoadedProcedure(FantasyMobsModElements instance) {
		super(instance, 35);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure OnModLoaded!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		FantasyMobsModVariables.MapVariables.get(world).DIMID_UNDERDARK = (double) (UnderdarkDimension.type.getId());
		FantasyMobsModVariables.MapVariables.get(world).syncData(world);
	}

	@SubscribeEvent
	public void onPlayerLoggedIn(PlayerEvent.PlayerLoggedInEvent event) {
		Entity entity = event.getPlayer();
		Map<String, Object> dependencies = new HashMap<>();
		dependencies.put("x", entity.getPosX());
		dependencies.put("y", entity.getPosY());
		dependencies.put("z", entity.getPosZ());
		dependencies.put("world", entity.world);
		dependencies.put("entity", entity);
		dependencies.put("event", event);
		this.executeProcedure(dependencies);
	}
}
