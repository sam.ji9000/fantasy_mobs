package net.mcreator.fantasymobs.procedures;

import net.minecraft.item.ItemStack;

import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;

@FantasyMobsModElements.ModElement.Tag
public class DrowWandBulletHitProcedure extends FantasyMobsModElements.ModElement {
	public DrowWandBulletHitProcedure(FantasyMobsModElements instance) {
		super(instance, 53);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("itemstack") == null) {
			System.err.println("Failed to load dependency itemstack for procedure DrowWandBulletHit!");
			return;
		}
		ItemStack itemstack = (ItemStack) dependencies.get("itemstack");
		((itemstack)).shrink((int) 1);
	}
}
