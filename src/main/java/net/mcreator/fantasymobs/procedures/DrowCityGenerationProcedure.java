package net.mcreator.fantasymobs.procedures;

import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.IWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.Rotation;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Mirror;

import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;

@FantasyMobsModElements.ModElement.Tag
public class DrowCityGenerationProcedure extends FantasyMobsModElements.ModElement {
	public DrowCityGenerationProcedure(FantasyMobsModElements instance) {
		super(instance, 61);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure DrowCityGeneration!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure DrowCityGeneration!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure DrowCityGeneration!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure DrowCityGeneration!");
			return;
		}
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double x_counter = 0;
		double z_counter = 0;
		double build_roll = 0;
		x_counter = (double) 0;
		z_counter = (double) 0;
		for (int index0 = 0; index0 < (int) (3); index0++) {
			for (int index1 = 0; index1 < (int) (3); index1++) {
				if ((!(((z_counter) == 1) && ((x_counter) == 1)))) {
					build_roll = (double) Math.random();
					if ((((build_roll) >= 0) && ((build_roll) < 0.15))) {
						if (!world.getWorld().isRemote) {
							Template template = ((ServerWorld) world.getWorld()).getSaveHandler().getStructureTemplateManager()
									.getTemplateDefaulted(new ResourceLocation("fantasy_mobs", "drow_temple"));
							if (template != null) {
								template.addBlocksToWorld(world,
										new BlockPos((int) (x + ((-16) + (16 * (x_counter)))), (int) y, (int) (z + ((-16) + (16 * (z_counter))))),
										new PlacementSettings().setRotation(Rotation.NONE).setMirror(Mirror.NONE).setChunk(null)
												.setIgnoreEntities(false));
							}
						}
					}
					if ((((build_roll) >= 0.15) && ((build_roll) < 0.3))) {
						if (!world.getWorld().isRemote) {
							Template template = ((ServerWorld) world.getWorld()).getSaveHandler().getStructureTemplateManager()
									.getTemplateDefaulted(new ResourceLocation("fantasy_mobs", "drow_armory"));
							if (template != null) {
								template.addBlocksToWorld(world,
										new BlockPos((int) (x + ((-16) + (16 * (x_counter)))), (int) y, (int) (z + ((-16) + (16 * (z_counter))))),
										new PlacementSettings().setRotation(Rotation.NONE).setMirror(Mirror.NONE).setChunk(null)
												.setIgnoreEntities(false));
							}
						}
					}
					if ((((build_roll) >= 0.3) && ((build_roll) < 0.55))) {
						if (!world.getWorld().isRemote) {
							Template template = ((ServerWorld) world.getWorld()).getSaveHandler().getStructureTemplateManager()
									.getTemplateDefaulted(new ResourceLocation("fantasy_mobs", "drow_prison"));
							if (template != null) {
								template.addBlocksToWorld(world,
										new BlockPos((int) (x + ((-16) + (16 * (x_counter)))), (int) y, (int) (z + ((-16) + (16 * (z_counter))))),
										new PlacementSettings().setRotation(Rotation.NONE).setMirror(Mirror.NONE).setChunk(null)
												.setIgnoreEntities(false));
							}
						}
					}
					if ((((build_roll) >= 0.55) && ((build_roll) < 0.8))) {
						if (!world.getWorld().isRemote) {
							Template template = ((ServerWorld) world.getWorld()).getSaveHandler().getStructureTemplateManager()
									.getTemplateDefaulted(new ResourceLocation("fantasy_mobs", "drow_library"));
							if (template != null) {
								template.addBlocksToWorld(world,
										new BlockPos((int) (x + ((-16) + (16 * (x_counter)))), (int) y, (int) (z + ((-16) + (16 * (z_counter))))),
										new PlacementSettings().setRotation(Rotation.NONE).setMirror(Mirror.NONE).setChunk(null)
												.setIgnoreEntities(false));
							}
						}
					}
					if (((build_roll) >= 0.8)) {
						if (!world.getWorld().isRemote) {
							Template template = ((ServerWorld) world.getWorld()).getSaveHandler().getStructureTemplateManager()
									.getTemplateDefaulted(new ResourceLocation("fantasy_mobs", "drow_farm"));
							if (template != null) {
								template.addBlocksToWorld(world,
										new BlockPos((int) (x + ((-16) + (16 * (x_counter)))), (int) y, (int) (z + ((-16) + (16 * (z_counter))))),
										new PlacementSettings().setRotation(Rotation.NONE).setMirror(Mirror.NONE).setChunk(null)
												.setIgnoreEntities(false));
							}
						}
					}
				}
				z_counter = (double) ((z_counter) + 1);
			}
			z_counter = (double) 0;
			x_counter = (double) ((x_counter) + 1);
		}
	}
}
