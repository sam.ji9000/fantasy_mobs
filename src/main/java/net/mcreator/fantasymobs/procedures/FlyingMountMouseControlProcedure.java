package net.mcreator.fantasymobs.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.World;
import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.entity.FlyingMountEntity;
import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;
import java.util.HashMap;

@FantasyMobsModElements.ModElement.Tag
public class FlyingMountMouseControlProcedure extends FantasyMobsModElements.ModElement {
	public FlyingMountMouseControlProcedure(FantasyMobsModElements instance) {
		super(instance, 102);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FlyingMountMouseControl!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (((entity.getRidingEntity()) instanceof FlyingMountEntity.CustomEntity)) {
			if (((((entity.getRidingEntity()).getSubmergedHeight()) == 0) && ((Math.abs(((entity.getRidingEntity()).getMotion().getX())) > 0.1)
					|| (Math.abs(((entity.getRidingEntity()).getMotion().getZ())) > 0.1)))) {
				(entity.getRidingEntity()).setMotion(((entity.getRidingEntity()).getMotion().getX()), ((entity.rotationPitch) * (-0.01)),
						((entity.getRidingEntity()).getMotion().getZ()));
			} else {
				if ((((entity.getRidingEntity()).getSubmergedHeight()) > 0)) {
					(entity.getRidingEntity()).setMotion(((entity.getRidingEntity()).getMotion().getX()), 0.1,
							((entity.getRidingEntity()).getMotion().getZ()));
				} else {
					(entity.getRidingEntity()).setMotion(((entity.getRidingEntity()).getMotion().getX()), 0,
							((entity.getRidingEntity()).getMotion().getZ()));
				}
			}
		}
	}

	@SubscribeEvent
	public void onPlayerTick(TickEvent.PlayerTickEvent event) {
		if (event.phase == TickEvent.Phase.END) {
			Entity entity = event.player;
			World world = entity.world;
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
