package net.mcreator.fantasymobs.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;

@FantasyMobsModElements.ModElement.Tag
public class FlyingMountOnInitialEntitySpawnProcedure extends FantasyMobsModElements.ModElement {
	public FlyingMountOnInitialEntitySpawnProcedure(FantasyMobsModElements instance) {
		super(instance, 46);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FlyingMountOnInitialEntitySpawn!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		entity.getPersistentData().putDouble("secondsUnmounted", 0);
	}
}
