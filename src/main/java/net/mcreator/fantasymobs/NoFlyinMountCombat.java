package net.mcreator.fantasymobs.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.World;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;

import net.mcreator.fantasymobs.entity.FlyingMountEntity;
import net.mcreator.fantasymobs.FantasyMobsModElements;

import java.util.Map;
import java.util.HashMap;

@FantasyMobsModElements.ModElement.Tag
public class NoFlyingMountCombatProcedure extends FantasyMobsModElements.ModElement {
	public NoFlyingMountCombatProcedure(FantasyMobsModElements instance) {
		super(instance, 100);
		MinecraftForge.EVENT_BUS.register(this);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("sourceentity") == null) {
			System.err.println("Failed to load dependency sourceentity for procedure NoFlyingMountCombat!");
			return;
		}
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((sourceentity instanceof ServerPlayerEntity) || (sourceentity instanceof PlayerEntity))) {
			if (((sourceentity.getRidingEntity()) instanceof FlyingMountEntity.CustomEntity)) {
				if (!(sourceentity.getRidingEntity()).world.isRemote)
					(sourceentity.getRidingEntity()).remove();
			}
		}
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingAttackEvent event) {
		Entity sourceentity = event.getSource().getTrueSource();
		if (event != null && event.getEntity() != null && sourceentity != null) {
			Entity entity = event.getEntity();
			double i = entity.getPosX();
			double j = entity.getPosY();
			double k = entity.getPosZ();
			double amount = event.getAmount();
			World world = entity.world;
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("amount", amount);
			dependencies.put("world", world);
			dependencies.put("entity", entity);
			dependencies.put("sourceentity", sourceentity);
			dependencies.put("event", event);
			this.executeProcedure(dependencies);
		}
	}
}
