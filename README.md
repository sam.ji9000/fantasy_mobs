# Dependencies
Mods this mod require for proper function:
- Toolkit: For spawning larger NBT structures https://www.curseforge.com/minecraft/mc-mods/tool-kit/files/2866642


In dev, dependencies should be a deoubfuscated .jar that should be included inside the fantasy_mobs/run/mods folder so you can use those mods while developing. 
The run folder is not pushed to the repo, so dependencies should be added to each local /run/mods folder

# Credits and links
- orc texture - https://www.planetminecraft.com/skin/orc-1911093/
- orc sound - https://freesound.org/people/chestnutjam/sounds/402072/
- silkder texture - https://www.planetminecraft.com/mob-skin/trippy-tarantula/
- drow texture - https://www.planetminecraft.com/skin/zakn-the-swift----drow-assassin/
- beholder texture - https://www.planetminecraft.com/mob-skin/beholder-ghast/
- flying mount texture - https://www.planetminecraft.com/mob-skin/prehistoric-dragonfly/
- drow mage texture - https://www.planetminecraft.com/skin/drow-ranger-dota-2-2019078/
- underlizard texture - https://www.planetminecraft.com/mob-skin/prehistoric-gecko-java/
- myconid texture - https://www.planetminecraft.com/mob-skin/mushroom-creeper/
- myconid champion texture - https://www.planetminecraft.com/mob-skin/red-mushroom-golem/