//Made with Blockbench by IGOR_LIVE96 (Blue Fox)
//Paste this code into your mod.

public static class ModelMinecraftPhantom extends ModelBase {
	private final ModelRenderer head;
	private final ModelRenderer body;
	private final ModelRenderer mainwing1;
	private final ModelRenderer wing2;
	private final ModelRenderer mainwing2;
	private final ModelRenderer wing1;
	private final ModelRenderer butt;
	private final ModelRenderer spike;

	public ModelMinecraftPhantom() {
		textureWidth = 64;
		textureHeight = 64;

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 2.0F, -13.3F);
		setRotationAngle(head, 0.0F, 0.0F, 0.0F);
		head.cubeList.add(new ModelBox(head, 0, 0, -3.0F, -2.0F, -5.0F, 7, 3, 5, 0.0F, true));

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 0.0F, -10.0F);
		body.cubeList.add(new ModelBox(body, 0, 8, -2.0F, -1.0F, -4.0F, 5, 3, 9, 0.0F, true));

		mainwing1 = new ModelRenderer(this);
		mainwing1.setRotationPoint(-3.0F, -1.0F, -9.0F);
		setRotationAngle(mainwing1, 0.0F, 0.0F, 0.0F);
		mainwing1.cubeList.add(new ModelBox(mainwing1, 23, 12, -5.0F, 0.0F, -5.0F, 6, 2, 9, 0.0F, true));

		wing2 = new ModelRenderer(this);
		wing2.setRotationPoint(-5.0F, 0.0F, 0.0F);
		setRotationAngle(wing2, 0.0F, 0.0F, 0.0F);
		mainwing1.addChild(wing2);
		wing2.cubeList.add(new ModelBox(wing2, 16, 24, -13.0F, 0.0F, -5.0F, 13, 1, 9, 0.0F, true));

		mainwing2 = new ModelRenderer(this);
		mainwing2.setRotationPoint(4.0F, -1.0F, -9.0F);
		setRotationAngle(mainwing2, 0.0F, 0.0F, 0.0F);
		mainwing2.cubeList.add(new ModelBox(mainwing2, 23, 12, -1.0F, 0.0F, -5.0F, 6, 2, 9, 0.0F, false));

		wing1 = new ModelRenderer(this);
		wing1.setRotationPoint(5.0F, 0.0F, 0.0F);
		setRotationAngle(wing1, 0.0F, 0.0F, 0.0F);
		mainwing2.addChild(wing1);
		wing1.cubeList.add(new ModelBox(wing1, 16, 24, 0.0F, 0.0F, -5.0F, 13, 1, 9, 0.0F, true));

		butt = new ModelRenderer(this);
		butt.setRotationPoint(1.0F, 0.0F, -5.0F);
		butt.cubeList.add(new ModelBox(butt, 3, 20, -2.0F, -1.0F, 0.0F, 3, 2, 6, 0.0F, true));

		spike = new ModelRenderer(this);
		spike.setRotationPoint(0.0F, -1.0F, 6.0F);
		butt.addChild(spike);
		spike.cubeList.add(new ModelBox(spike, 4, 29, -1.0F, 0.0F, 0.0F, 1, 1, 6, 0.0F, true));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		head.render(f5);
		body.render(f5);
		mainwing1.render(f5);
		mainwing2.render(f5);
		butt.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.mainwing2.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		this.wing1.rotateAngleZ = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		this.wing2.rotateAngleZ = MathHelper.cos(f * 0.6662F) * f1;
		this.mainwing1.rotateAngleZ = MathHelper.cos(f * 0.6662F) * f1;
	}
}