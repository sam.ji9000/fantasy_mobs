// Made with Blockbench 3.5.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class ModelhookHorror_v2 extends EntityModel<Entity> {
	private final ModelRenderer body;
	private final ModelRenderer head;
	private final ModelRenderer rightArm;
	private final ModelRenderer leftArm;
	private final ModelRenderer rightLeg;
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer leftLeg;
	private final ModelRenderer bone6;
	private final ModelRenderer bone5;
	private final ModelRenderer bone4;

	public ModelhookHorror_v2() {
		textureWidth = 64;
		textureHeight = 64;

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 2.0F, 0.0F);
		body.setTextureOffset(0, 0).addBox(-4.0F, 0.0F, 0.0F, 8.0F, 11.0F, 6.0F, 0.0F, false);

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 2.0F, 0.0F);
		head.setTextureOffset(0, 17).addBox(-3.0F, -5.0F, -5.0F, 6.0F, 5.0F, 5.0F, 0.0F, false);
		head.setTextureOffset(17, 17).addBox(-2.0F, -2.0F, 0.0F, 4.0F, 2.0F, 3.0F, 0.0F, false);
		head.setTextureOffset(12, 27).addBox(-2.0F, -3.0F, -6.0F, 4.0F, 3.0F, 1.0F, 0.0F, false);
		head.setTextureOffset(34, 34).addBox(-1.0F, -2.0F, -7.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		head.setTextureOffset(28, 12).addBox(-1.0F, -1.0F, -8.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		rightArm = new ModelRenderer(this);
		rightArm.setRotationPoint(4.0F, 3.0F, 1.0F);
		rightArm.setTextureOffset(31, 12).addBox(-11.0F, -1.0F, -1.0F, 3.0F, 9.0F, 3.0F, 0.0F, false);
		rightArm.setTextureOffset(40, 0).addBox(-10.0F, 14.0F, -4.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rightArm.setTextureOffset(42, 1).addBox(-10.0F, 12.0F, -5.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rightArm.setTextureOffset(42, 1).addBox(-10.0F, 10.0F, -4.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rightArm.setTextureOffset(42, 1).addBox(-10.0F, 8.0F, -3.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		leftArm = new ModelRenderer(this);
		leftArm.setRotationPoint(-4.0F, 3.0F, 1.0F);
		leftArm.setTextureOffset(28, 0).addBox(8.0F, -1.0F, -1.0F, 3.0F, 9.0F, 3.0F, 0.0F, false);
		leftArm.setTextureOffset(42, 1).addBox(9.0F, 8.0F, -3.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		leftArm.setTextureOffset(42, 1).addBox(9.0F, 10.0F, -4.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		leftArm.setTextureOffset(42, 1).addBox(9.0F, 12.0F, -5.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		leftArm.setTextureOffset(40, 0).addBox(9.0F, 14.0F, -4.0F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		rightLeg = new ModelRenderer(this);
		rightLeg.setRotationPoint(-2.0F, 13.0F, 0.0F);
		rightLeg.setTextureOffset(0, 27).addBox(-2.0F, 0.0F, 0.0F, 3.0F, 11.0F, 3.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(-2.0F, 11.0F, -2.0F);
		rightLeg.addChild(bone);
		setRotationAngle(bone, 0.0F, 0.7854F, 0.0F);
		bone.setTextureOffset(0, 0).addBox(-1.0F, -1.0F, 0.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rightLeg.addChild(bone2);
		bone2.setTextureOffset(0, 0).addBox(-1.0F, 10.0F, -3.0F, 1.0F, 1.0F, 3.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(1.0F, 10.0F, -2.0F);
		rightLeg.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -0.7854F, 0.0F);
		bone3.setTextureOffset(0, 0).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		leftLeg = new ModelRenderer(this);
		leftLeg.setRotationPoint(2.0F, 13.0F, 0.0F);
		leftLeg.setTextureOffset(22, 22).addBox(-1.0F, 0.0F, 0.0F, 3.0F, 11.0F, 3.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(-3.0F, 10.0F, -2.0F);
		leftLeg.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -0.7854F, 0.0F);
		bone6.setTextureOffset(0, 0).addBox(3.5355F, 0.0F, -3.5355F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(-4.0F, 0.0F, 0.0F);
		leftLeg.addChild(bone5);
		bone5.setTextureOffset(0, 0).addBox(4.0F, 10.0F, -3.0F, 1.0F, 1.0F, 3.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(-6.0F, 11.0F, -2.0F);
		leftLeg.addChild(bone4);
		setRotationAngle(bone4, 0.0F, 0.7854F, 0.0F);
		bone4.setTextureOffset(0, 0).addBox(2.5355F, -1.0F, 3.5355F, 1.0F, 1.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		body.render(matrixStack, buffer, packedLight, packedOverlay);
		head.render(matrixStack, buffer, packedLight, packedOverlay);
		rightArm.render(matrixStack, buffer, packedLight, packedOverlay);
		leftArm.render(matrixStack, buffer, packedLight, packedOverlay);
		rightLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		leftLeg.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.rightLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
		this.rightArm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float) Math.PI) * f1;
		this.leftArm.rotateAngleX = MathHelper.cos(f * 0.6662F) * f1;
		this.leftLeg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
	}
}